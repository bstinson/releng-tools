#!/usr/bin/bash

set -euo pipefail
IFS=$'\n\t'

if [[ $USER != 'compose' ]]; then
    echo "This script must be run as the compose user."
    exit 1
fi

COMPOSE="latest-CentOS-Stream"
CENTOS_RELEASE="8-stream"

DRY_RUN=$1

function message() {
    echo -e "\e[36m* $1\e[m"
}


function sync_rpms() {
    arch=$1
    shift
    variants=($@)
    for variant in ${variants[@]}; do
        VARIANT_DIR=/mnt/centos/staged/$CENTOS_RELEASE/$variant/$arch/os/
        mkdir -p ${VARIANT_DIR}
        pushd ${VARIANT_DIR}
        if [ $DRY_RUN ]; then
            rsync -avhH --info progress2 --link-dest=/mnt/centos --dry-run \
                /mnt/centos/composes/stream-8/production/$COMPOSE/compose/$variant/$arch/os/ ${VARIANT_DIR}
        else
            message "syncing $variant $arch"
            # delete old comps files first to ensure we use the new ones
            rm -vf ${VARIANT_DIR}/repodata/*-comps-*.xml*
            # rsync repo from compose
            rsync -avhH --info progress2 --link-dest=/mnt/centos \
                /mnt/centos/composes/stream-8/production/$COMPOSE/compose/$variant/$arch/os/ ${VARIANT_DIR}/
            # modular metadata must be decompressed for createrepo_c to pick it up
            # https://github.com/rpm-software-management/createrepo_c/issues/263
            # update repodata, make sure modules work
            find repodata -type f -name '*-modules.yaml.gz' -exec gunzip -vfk {} \;
            find repodata -type f -name '*-modules.yaml.xz' -exec unxz -vfk {} \;
            createrepo_c --update --workers 8 --xz \
                --distro 'cpe:/o:centos-stream:centos-stream:8,CentOS Stream 8' \
                --revision 8-stream \
                --groupfile $(find repodata -type f -name '*-comps-*.xml') \
                --retain-old-md-by-age 1m \
                .
            ## Uncomment this when you are ready to trim down the duplicates
            ## Use updated repodata to clean out old rpms, but keep the newest 5
            #dnf --quiet repomanage --old --keep 5 . 2>/dev/null | while read pkg ; do
            #  echo "Removing old package: ${pkg}"
            #  rm -f ${pkg}
            #done
            ## update the repodata again
            #find repodata -type f -name '*-modules.yaml.gz' -exec gunzip -vfk {} \;
            #find repodata -type f -name '*-modules.yaml.xz' -exec unxz -vfk {} \;
            #createrepo_c --update --workers 8 --xz \
            #    --distro 'cpe:/o:centos-stream:centos-stream:8,CentOS Stream 8' \
            #    --revision 8-stream \
            #    --groupfile $(find repodata -type f -name '*-comps-*.xml') \
            #    --retain-old-md-by-age 1m \
            #    .
        fi
        popd
    done
}

function sync_isos() {
    arch=$1
    message "syncing isos $arch"
    mkdir -p  /mnt/centos/staged/$CENTOS_RELEASE/isos/$arch/
    pushd /mnt/centos/staged/$CENTOS_RELEASE/isos/$arch/
    if [ $DRY_RUN ]; then
        rsync -avhH --info progress2  --delete --link-dest=/mnt/centos --dry-run \
            /mnt/centos/composes/stream-8/production/$COMPOSE/compose/BaseOS/$arch/iso/ ./
    else
        rsync -avhH --info progress2  --delete --link-dest=/mnt/centos \
            /mnt/centos/composes/stream-8/production/$COMPOSE/compose/BaseOS/$arch/iso/ ./
        # set up latest symlinks
        for f in *.iso*; do ln -sf $f $(sed -r "s/-[0-9]{8}.[0-9]-$arch-/-$arch-latest-/" <<< $f); done
        # modify checksum file
        cp SHA256SUM CHECKSUM
        sed -r "s/-[0-9]{8}.[0-9]-$arch-/-$arch-latest-/" CHECKSUM > CHECKSUM.latest
        mv CHECKSUM CHECKSUM.date
        cat CHECKSUM.date CHECKSUM.latest > CHECKSUM
        # Cleanup
        rm CHECKSUM.date CHECKSUM.latest
        rm *SHA256SUM *SHA1SUM *MD5SUM
    fi
    popd
}

function sync_debug_rpms() {
    arch=$1
    shift
    variants=($@)
    message "syncing debug $arch"
    mkdir -p  /mnt/centos/staged/debug/$CENTOS_RELEASE/$arch/
    pushd /mnt/centos/staged/debug/$CENTOS_RELEASE/$arch/
    for variant in ${variants[@]}; do
        if [ $DRY_RUN ]; then
            rsync -avhH --info progress2 --link-dest=/mnt/centos --dry-run \
                /mnt/centos/composes/stream-8/production/$COMPOSE/compose/$variant/$arch/debug/tree/Packages/ Packages/
        else
            rsync -avhH --info progress2 --link-dest=/mnt/centos \
                /mnt/centos/composes/stream-8/production/$COMPOSE/compose/$variant/$arch/debug/tree/Packages/ Packages/
        fi
    done
    createrepo_c --update --workers 8 --xz .
    popd
}


function sync_source_rpms() {
    variants=($@)
    for variant in ${variants[@]}; do
        mkdir -p  /mnt/centos/staged/vault/$CENTOS_RELEASE/$variant/Source/
        pushd /mnt/centos/staged/vault/$CENTOS_RELEASE/$variant/Source/
        if [ $DRY_RUN ]; then
            rsync -avhH --info progress2 --link-dest=/mnt/centos --dry-run \
                /mnt/centos/composes/stream-8/production/$COMPOSE/compose/$variant/source/tree/ SPackages/
        else
            message "syncing source $variant"
            rsync -avhH --info progress2 --link-dest=/mnt/centos \
                /mnt/centos/composes/stream-8/production/$COMPOSE/compose/$variant/source/tree/ SPackages/
            createrepo_c --update --workers 8 --xz .
            ## Uncomment this when you are ready to trim down the duplicates
            ## Use updated repodata to clean out old rpms, but keep the newest 5
            #dnf --quiet repomanage --old --keep 5 . 2>/dev/null | while read pkg ; do
            #  echo "Removing old package: ${pkg}"
            #  rm -f ${pkg}
            #done
            #createrepo_c --update --workers 8 --xz .
        fi
        popd
    done
}

pushd ${HOME} # We're running as the compose user so we want to be in a CWD that we can go back and forth to

sync_rpms aarch64 BaseOS AppStream PowerTools HighAvailability
sync_rpms ppc64le BaseOS AppStream PowerTools HighAvailability ResilientStorage
sync_rpms x86_64  BaseOS AppStream PowerTools HighAvailability ResilientStorage NFV RT

sync_isos aarch64
sync_isos ppc64le
sync_isos x86_64

sync_debug_rpms aarch64 BaseOS AppStream PowerTools HighAvailability
sync_debug_rpms ppc64le BaseOS AppStream PowerTools HighAvailability ResilientStorage
sync_debug_rpms x86_64  BaseOS AppStream PowerTools HighAvailability ResilientStorage RT NFV

sync_source_rpms BaseOS AppStream PowerTools HighAvailability ResilientStorage RT NFV

jq -r '.payload.compose.id' \
    /mnt/centos/composes/stream-8/production/$COMPOSE/compose/metadata/composeinfo.json \
    > /mnt/centos/staged/$CENTOS_RELEASE/COMPOSE_ID
