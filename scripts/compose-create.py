#!/usr/bin/python3

import argparse
import json
import logging
import os

from odcs.client.odcs import ODCS, AuthMech, ComposeSourceBuild, ComposeSourceTag, ComposeSourceRawConfig

try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument('-g', '--gitrepo', help="Which gitlab repo to use",
                    choices=["c9s", "c9s_development", "c9s_test", "c9s_bstinson", "c9s_tdawson", "c9s_jboyer"])
parser.add_argument('-b', '--branch', default='centos-9-stream', help="What git branch to use")
parser.add_argument('-t', '--type', default='test', help="What compose type to run",
                    choices=["test", "development", "production"])
parser.add_argument('-d', '--days', help="Days until expire")
parser.add_argument('-l', '--label', help="Compose label (production only)")
args = parser.parse_args()

if not args.gitrepo:
    git_repo = "c9s"
else:
    git_repo = args.gitrepo

# Production composes must have a label
if args.type == "production":
    if not args.label:
        raise SystemExit("production composes require a label")

# Development composes are c9s_development by default
if args.type == "development":
    if not args.gitrepo:
        git_repo = "c9s_development"

# Test composes are c9s_test by default
if args.type == "test":
    if not args.gitrepo:
        git_repo = "c9s_test"

# Set the default time_to_expire (TTL)
if args.days:
    days=int(args.days)
elif args.type == "production":
    days=21
elif args.type == "development":
    days=14
elif args.type == "test":
    days=4
else:
    days=3
seconds_added=days * 86400

# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

od = ODCS("https://odcs.stream.rdu2.redhat.com/", auth_mech=AuthMech.Kerberos)
build = ComposeSourceRawConfig(git_repo, commit=args.branch)
if args.type == "production":
    compose = od.request_compose(build, compose_type=args.type, seconds_to_live=seconds_added, label=args.label)
else:
    compose = od.request_compose(build, compose_type=args.type, seconds_to_live=seconds_added)

print(compose)
print(git_repo, args.branch, args.type)
# Write this out to a file
work_dir = os.getcwd()+"/"
with open(work_dir+'compose.txt', 'a') as f:
    f.write("%s %s %s %s\n" % (str(compose['id']), git_repo, args.branch, args.type))
with open(work_dir+'response.json', 'a') as f:
    json.dump(compose, f)

