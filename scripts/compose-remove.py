#!/usr/bin/python3

import logging
import argparse

from odcs.client.odcs import ODCS, AuthMech, ComposeSourceBuild, ComposeSourceTag, ComposeSourceRawConfig

try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument("compose_number", help="What compose number to remove")
args = parser.parse_args()
composeNumber = args.compose_number


# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

od = ODCS("https://odcs.stream.rdu2.redhat.com/", auth_mech=AuthMech.Kerberos)
try:
    compose = od.delete_compose(args.compose_number)
    print(compose)
except Exception as e:
    print(e)


